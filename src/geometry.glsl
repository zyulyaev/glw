#version 410

layout(points) in;
layout(line_strip, max_vertices = 101) out;

void main() {
	vec4 center = gl_in[0].gl_Position;
	float pi2 = atan(0, -1) * 2;
	for (int i = 0; i <= 100; ++i) {
		gl_Position = center + vec4(cos(pi2 / 100 * i), sin(pi2 / 100 * i), 0, 0) / 4;
		EmitVertex();
	}

	EndPrimitive();
}