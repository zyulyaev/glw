#include <cstdio>

#define HPP(src) #src
#define MAX_LEN 1000

const char* hpp_header = "#pragma once\n\n" HPP(
namespace res {\n
\tstruct Resource {\n
\t\tchar const* data;\n
\t\tsize_t const length;\n
\t};\n\n
);

const char* hpp_footer = HPP(
}
);

const char* ss_header = "section .rodata\n\n";
const char* ss_footer = "";

int main(int argc, char** argv) {
	FILE* ss = fopen(argv[1], "w");
	FILE* hpp = fopen(argv[2], "w");

	fprintf(ss, "%s", ss_header);
	fprintf(hpp, "%s", hpp_header);

	char line[MAX_LEN];
	char ns[MAX_LEN], alias[MAX_LEN], filename[MAX_LEN];
	for (size_t ln = 0; fgets(line, MAX_LEN - 1, stdin); ln++) {
		if (line[0] == '#' || line[0] == '\n') continue;
		if (sscanf(line, "%s%s%s", ns, alias, filename) != 3) {
			fprintf(stderr, "bad line #%zu: %s\n", ln, line);
			break;
		}

		fprintf(ss, "global _%s\n", alias);
		fprintf(ss, "%s_data:\n", alias);
		fprintf(ss, "\tincbin \"%s\"\n", filename);
		fprintf(ss, "%s_end:\n", alias);
		fprintf(ss, "\tdb 0\n");
		fprintf(ss, "_%s:\n", alias);
		fprintf(ss, "\tdq %s_data\n", alias);
		fprintf(ss, "\tdq %s_end - %s_data\n", alias, alias);
		fprintf(ss, "\n");

		fprintf(hpp, "\tnamespace %s { extern \"C\" Resource %s; }\n", ns, alias);
	}

	fprintf(ss, "%s", ss_footer);
	fprintf(hpp, "%s", hpp_footer);

	fclose(ss);
	fclose(hpp);
}