#version 410

in vec3 pos;
in vec3 color;
in vec2 texcoord;

out vec3 Color;
out vec2 Texcoord;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
uniform vec3 overrideColor;

void main() {
	Color = overrideColor * color;
	Texcoord = texcoord;
	gl_Position = proj * view * model * vec4(pos, 1);
}