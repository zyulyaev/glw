#version 410

in vec3 Color;
in vec2 Texcoord;

out vec4 color;

uniform sampler2D texKitten;
uniform sampler2D texPuppy;

void main() {
	color = vec4(Color, 1) * mix(texture(texKitten, Texcoord), texture(texPuppy, Texcoord), 0.5);
}