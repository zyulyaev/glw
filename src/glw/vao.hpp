#pragma once

#include "util.hpp"
#include "object.hpp"
#include "context.hpp"

namespace glw {

namespace {
	struct vertex_array_functions {
		static constexpr gl_object_generator*& generator = glGenVertexArrays;
		static constexpr gl_object_deleter*& deleter = glDeleteVertexArrays;
	};
}

typedef gl_object<vertex_array_functions> vertex_array;
typedef gl_object_pool<vertex_array> vertex_array_pool;
class vertex_array_target;
class vertex_array_extension;

enum class component_type : GLenum {
	UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
	BYTE = GL_BYTE,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
	SHORT = GL_SHORT,
	UNSIGNED_INT = GL_UNSIGNED_INT,
	INT = GL_INT,
	HALF_FLOAT = GL_HALF_FLOAT,
	FLOAT = GL_FLOAT,
	DOUBLE = GL_DOUBLE,
	FIXED = GL_FIXED
};

enum class drawing_mode : GLenum {
	POINTS = GL_POINTS,
	LINE_STRIP = GL_LINE_STRIP,
	LINE_LOOP = GL_LINE_LOOP,
	LINES = GL_LINES,
	TRIANGLE_STRIP = GL_TRIANGLE_STRIP,
	TRIANGLE_FAN = GL_TRIANGLE_FAN,
	TRIANGLES = GL_TRIANGLES,
	PATCHES = GL_PATCHES,
	LINES_ADJACENCY = GL_LINES_ADJACENCY,
	TRIANGLES_ADJACENCY = GL_TRIANGLES_ADJACENCY,
	LINE_STRIP_ADJACENCY = GL_LINE_STRIP_ADJACENCY,
	TRIANGLE_STRIP_ADJACENCY = GL_TRIANGLE_STRIP_ADJACENCY
};

class vertex_array_target : gl_object_target<vertex_array> {
public:
	vertex_array_target& operator=(vertex_array& array) {
		glBindVertexArray(_getName(array));
		__check_opengl_error();
		return (*this);
	}

	vertex_array_target& enable_vertex_attrib_array(GLint attribLocation) {
		glEnableVertexAttribArray(attribLocation);
		__check_opengl_error();
		return (*this);
	}

	vertex_array_target& set_vertex_attrib_pointer(GLint attribLocation, GLint size, component_type type,
								GLboolean normalized, GLsizei stride, void const* pointer) {
		glVertexAttribPointer(attribLocation, size, static_cast<GLenum>(type), normalized, stride, pointer);
		__check_opengl_error();
		return (*this);
	}

	vertex_array_target& draw_arrays(drawing_mode mode, GLint first, GLsizei count) {
		glDrawArrays(static_cast<GLenum>(mode), first, count);
		__check_opengl_error();
		return (*this);
	}

	vertex_array_target& draw_elements(drawing_mode mode, GLsizei count, component_type type, GLvoid const* indices) {
		glDrawElements(static_cast<GLenum>(mode), count, static_cast<GLenum>(type), indices);
		__check_opengl_error();
		return (*this);
	}

private:
	vertex_array_target() = default;

	friend class vertex_array_extension;
};

class vertex_array_extension : public virtual context_extension<> {
public:
	vertex_array_target active_vertex_array;
};

}