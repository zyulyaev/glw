#pragma once

#include "util.hpp"
#include "object.hpp"
#include "context.hpp"

namespace glw {

namespace {
	struct buffer_functions {
		static constexpr gl_object_generator*& generator = glGenBuffers;
		static constexpr gl_object_deleter*& deleter = glDeleteBuffers;
	};

	template<typename T>
	struct buffer_parameter_accessors {};

	template<>
	struct buffer_parameter_accessors<GLint> {
		static constexpr gl_param_getter<GLint>*& getter = glGetBufferParameteriv;
	};

	template<>
	struct buffer_parameter_accessors<GLint64> {
		static constexpr gl_param_getter<GLint64>*& getter = glGetBufferParameteri64v;
	};
}

typedef gl_object<buffer_functions> buffer;
typedef gl_object_pool<buffer> buffer_pool;
template<GLenum _target> class buffer_target;
class buffer_extension;

enum class buffer_usage : GLenum {
	DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
	DYNAMIC_READ = GL_DYNAMIC_READ,
	DYNAMIC_COPY = GL_DYNAMIC_COPY,

	STATIC_DRAW = GL_STATIC_DRAW,
	STATIC_READ = GL_STATIC_READ,
	STATIC_COPY = GL_STATIC_COPY,

	STREAM_DRAW = GL_STREAM_DRAW,
	STREAM_READ = GL_STREAM_READ,
	STREAM_COPY = GL_STREAM_COPY
};

enum class buffer_access : GLint {
	READ_ONLY = GL_READ_ONLY,
	WRITE_ONLY = GL_WRITE_ONLY,
	READ_WRITE = GL_READ_WRITE
};

template<GLenum _target>
class buffer_target : gl_object_target<buffer> {

template<GLenum _name, typename... Ts>
using buffer_param = gl_object_parameter<buffer_parameter_accessors, _target, _name, Ts...>;

public:
	buffer_target& operator=(buffer& buf) {
		glBindBuffer(_target, _getName(buf));
		return (*this);
	}

	buffer_target& loadData(GLsizeiptr size, const void* data, buffer_usage usage) {
		glBufferData(_target, size, data, static_cast<GLenum>(usage));
		__check_opengl_error();
		return (*this);
	}

	buffer_param<GL_BUFFER_SIZE, GLint, GLint64> size;
	buffer_param<GL_BUFFER_ACCESS, buffer_access> access;
	buffer_param<GL_BUFFER_USAGE, buffer_usage> usage;
	buffer_param<GL_BUFFER_MAPPED, GLint> mapped;
private:
	buffer_target() = default;

	friend class buffer_extension;
};

class buffer_extension : public virtual context_extension<> {
public:
	buffer_target<GL_ARRAY_BUFFER> buffer_array;
	buffer_target<GL_UNIFORM_BUFFER> buffer_uniform;
	buffer_target<GL_ATOMIC_COUNTER_BUFFER> buffer_atomic_counter;
	buffer_target<GL_COPY_READ_BUFFER> buffer_copy_read;
	buffer_target<GL_COPY_WRITE_BUFFER> buffer_copy_write;
	buffer_target<GL_DISPATCH_INDIRECT_BUFFER> buffer_dispatch_indirect;
	buffer_target<GL_DRAW_INDIRECT_BUFFER> buffer_draw_indirect;
	buffer_target<GL_ELEMENT_ARRAY_BUFFER> buffer_element_array;
	buffer_target<GL_PIXEL_PACK_BUFFER> buffer_pixel_pack;
	buffer_target<GL_PIXEL_UNPACK_BUFFER> buffer_pixel_unpack;
	buffer_target<GL_QUERY_BUFFER> buffer_query;
	buffer_target<GL_TEXTURE_BUFFER> buffer_texture;
	buffer_target<GL_SHADER_STORAGE_BUFFER> buffer_shader_storage;
	buffer_target<GL_TRANSFORM_FEEDBACK_BUFFER> buffer_transform_feedback;
};

}