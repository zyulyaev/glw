#pragma once

#include "util.hpp"
#include "context.hpp"
#include "common.hpp"

namespace glw {

class clear_color;
class framebuffer;
class framebuffer_extension;

enum class clear_buffer : GLbitfield {
	COLOR = GL_COLOR_BUFFER_BIT,
	DEPTH = GL_DEPTH_BUFFER_BIT,
	STENCIL = GL_STENCIL_BUFFER_BIT
};

enum class stencil_operation : GLenum {
	KEEP = GL_KEEP,
	ZERO = GL_ZERO,
	REPLACE = GL_REPLACE,
	INCREASE = GL_INCR,
	DECREASE = GL_DECR,
	INVERT = GL_INVERT,
	INCREASE_WRAP = GL_INCR_WRAP,
	DECREASE_WRAP = GL_DECR_WRAP
};

class clear_color : non_copyable {
public:
	clear_color& operator=(glm::vec4 const& color) {
		glClearColor(color.r, color.g, color.b, color.a);
		__check_opengl_error();
		return (*this);
	}

	operator glm::vec4() const {
		glm::vec4 result;
		glGetFloatv(GL_COLOR_CLEAR_VALUE, glm::value_ptr(result));
		__check_opengl_error();
		return result;
	}
private:
	clear_color() = default;
	friend class framebuffer;
};

class stencil_test : non_copyable {
public:
	gl_capability<GL_STENCIL_TEST> enabled;

	stencil_test& set_function(compare_function function, GLint reference, GLuint mask) {
		glStencilFunc(static_cast<GLenum>(function), reference, mask);
		__check_opengl_error();
		return (*this);
	}

	stencil_test& set_operation(stencil_operation stencil_fail, stencil_operation depth_fail, stencil_operation depth_pass) {
		glStencilOp(static_cast<GLenum>(stencil_fail), static_cast<GLenum>(depth_fail), static_cast<GLenum>(depth_pass));
		__check_opengl_error();
		return (*this);
	}

	stencil_test& set_mask(GLuint mask) {
		glStencilMask(mask);
		__check_opengl_error();
		return (*this);
	}

private:
	stencil_test() = default;

	friend class framebuffer;
};

class depth_test : non_copyable {
public:
	gl_capability<GL_DEPTH_TEST> enabled;

	depth_test& set_mask(GLboolean mask) {
		glDepthMask(mask);
		__check_opengl_error();
		return (*this);
	}
private:
	depth_test() = default;

	friend class framebuffer;
};


class framebuffer {
public:
	clear_color clear_color;
	stencil_test stencil_test;
	depth_test depth_test;

	template<typename... Ts>
	typename std::enable_if<if_all<std::is_same<clear_buffer, Ts>::value...>::value>::type clear(Ts... modes) {
		glClear(reduce([](GLbitfield a, GLbitfield b) {return a | b;}, 0u, static_cast<GLbitfield>(modes)...));
	}
};

class framebuffer_extension : public virtual context_extension<> {
public:
	framebuffer framebuffer;
};

}