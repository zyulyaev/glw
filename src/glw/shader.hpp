#pragma once

#include "util.hpp"

namespace glw {

class shader;
class shader_program;
class shader_program_target;
class shader_extension;

enum class shader_type : GLenum {
	COMPUTE = GL_COMPUTE_SHADER,
	FRAGMENT = GL_FRAGMENT_SHADER,
	VERTEX = GL_VERTEX_SHADER,
	GEOMETRY = GL_GEOMETRY_SHADER,
	TESS_EVALUATION = GL_TESS_EVALUATION_SHADER,
	TESS_CONTROL = GL_TESS_CONTROL_SHADER
};

class shader : non_copyable {
public:
	shader(shader && shader) : _name(shader._name) {
		shader._name = 0;
	}

	shader& operator=(shader shader) {
		std::swap(_name, shader._name);
		return (*this);
	}

	~shader() {
		if (_name != 0) {
			glDeleteShader(_name);
			__check_opengl_error();
		}
	}

	static shader compile(shader_type type, char const* source) {
		shader s = glCreateShader(static_cast<GLenum>(type));
		__check_opengl_error();
		glShaderSource(s._name, 1, &source, nullptr);
		__check_opengl_error();
		glCompileShader(s._name);
		__check_opengl_error();

		GLint status;
		glGetShaderiv(s._name, GL_COMPILE_STATUS, &status);
		if (status == GL_FALSE) {
			GLint logLength;
			glGetShaderiv(s._name, GL_INFO_LOG_LENGTH, &logLength);

			GLchar *rawMessage = new GLchar[logLength + 1];
			glGetShaderInfoLog(s._name, logLength, NULL, rawMessage);
			std::string message = rawMessage;
			delete[] rawMessage;

			throw std::runtime_error("shader compile failed: " + message);
		}

		return s;
	}
private:
	shader(GLuint name) : _name(name) {}

	GLuint _name;

	friend class shader_program;
};

class shader_program : non_copyable {
public:
	shader_program(shader_program && program) : _name(program._name) {
		program._name = 0;
	}
	shader_program& operator=(shader_program program) {
		std::swap(_name, program._name);
		return (*this);
	}
	~shader_program() {
		if (_name != 0) {
			glDeleteProgram(_name);
		}
	}

	GLint get_attrib_location(std::string const& name) {
		return glGetAttribLocation(_name, name.c_str());
	}

	GLint get_uniform_location(std::string const& name) {
		return glGetUniformLocation(_name, name.c_str());
	}

	shader_program& set_uniform(GLint location, GLint value) {
		glProgramUniform1i(_name, location, value);
		return (*this);
	}

	shader_program& set_uniform(GLint location, glm::vec2 const& value) {
		glProgramUniform2fv(_name, location, 1, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	shader_program& set_uniform(GLint location, glm::vec3 const& value) {
		glProgramUniform3fv(_name, location, 1, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	shader_program& set_uniform(GLint location, glm::vec4 const& value) {
		glProgramUniform4fv(_name, location, 1, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	shader_program& set_uniform(GLint location, glm::mat4 const& value, GLboolean transpose = false) {
		glProgramUniformMatrix4fv(_name, location, 1, transpose, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	template<typename... Args>
	static shader_program link(Args... args) {
		return link(__vararg<shader>(std::move(args)...));
	}

	static shader_program link(std::vector<shader> const& shaders) {
		shader_program program = glCreateProgram();
		__check_opengl_error();

		for (shader const& shader : shaders) {
			glAttachShader(program._name, shader._name);
			__check_opengl_error();
		}

		glLinkProgram(program._name);
		__check_opengl_error();

		for (shader const& shader : shaders) {
			glDetachShader(program._name, shader._name);
			__check_opengl_error();
		}

		GLint status;
		glGetProgramiv (program._name, GL_LINK_STATUS, &status);
		if (status == GL_FALSE) {
			GLint logLength;
			glGetProgramiv(program._name, GL_INFO_LOG_LENGTH, &logLength);

			GLchar *rawMessage = new GLchar[logLength + 1];
			glGetProgramInfoLog(program._name, logLength, NULL, rawMessage);
			std::string message = rawMessage;
			delete[] rawMessage;

			throw std::runtime_error("Link failure: " + message);
		}

		return program;
	}

private:
	shader_program(GLuint name) : _name(name) {}

	GLuint _name;

	friend class shader_program_target;
};

class shader_program_target : non_copyable {
public:
	shader_program_target& operator=(shader_program& program) {
		glUseProgram(program._name);
		__check_opengl_error();
		return (*this);
	}

	shader_program_target& set_uniform(GLint location, glm::vec2 const& value) {
		glUniform2fv(location, 1, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	shader_program_target& set_uniform(GLint location, glm::vec3 const& value) {
		glUniform3fv(location, 1, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	shader_program_target& set_uniform(GLint location, glm::vec4 const& value) {
		glUniform4fv(location, 1, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

	shader_program_target& set_uniform(GLint location, glm::mat4 const& value, GLboolean transpose = false) {
		glUniformMatrix4fv(location, 1, transpose, glm::value_ptr(value));
		__check_opengl_error();
		return (*this);
	}

private:
	shader_program_target() = default;

	friend class shader_extension;
};

class shader_extension : public virtual context_extension<> {
public:
	shader_program_target active_program;
};

}