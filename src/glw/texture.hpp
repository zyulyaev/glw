#pragma once

#include "util.hpp"
#include "object.hpp"
#include "common.hpp"

namespace glw {

namespace {
	struct texture_functions {
		static constexpr gl_object_generator& generator = glGenTextures;
		static constexpr gl_object_deleter& deleter = glDeleteTextures;
	};

	template<typename T>
	struct texture_parameter_getters;

	template<>
	struct texture_parameter_getters<GLint> {
		static constexpr gl_param_getter<GLint>& getter = glGetTexParameteriv;
	};

	template<>
	struct texture_parameter_getters<GLfloat> {
		static constexpr gl_param_getter<GLfloat>& getter = glGetTexParameterfv;
	};

	template<typename T>
	struct texture_parameter_accessors;

	template<>
	struct texture_parameter_accessors<GLint> : texture_parameter_getters<GLint> {
		static constexpr gl_param_setter<GLint>& setter = glTexParameteri;
		static constexpr gl_param_setter<GLint const*>& array_setter = glTexParameteriv;
	};

	template<>
	struct texture_parameter_accessors<GLfloat> : texture_parameter_getters<GLfloat> {
		static constexpr gl_param_setter<GLfloat>& setter = glTexParameterf;
		static constexpr gl_param_setter<GLfloat const*>& array_setter = glTexParameterfv;
	};
}

typedef gl_object<texture_functions> texture;
typedef gl_object_pool<texture> texture_pool;
template<GLenum _target> class texture_target;
class active_texture;
class texture_extension;

#ifdef GL_STENCIL_COMPONENT
enum class depth_stencil_texture_mode : GLenum {
	DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
	STENCIL_COMPONENT = GL_STENCIL_COMPONENT
};
#endif

enum class compare_mode : GLint {
	REF_TO_TEXTURE = GL_COMPARE_REF_TO_TEXTURE,
	NONE = GL_NONE
};

enum class min_filter : GLint {
	NEAREST = GL_NEAREST,
	LINEAR = GL_LINEAR,
	NEAREST_MIPMAP_NEAREST = GL_NEAREST_MIPMAP_NEAREST,
	LINEAR_MIPMAP_NEAREST = GL_LINEAR_MIPMAP_NEAREST,
	NEAREST_MIPMAP_LINEAR = GL_NEAREST_MIPMAP_LINEAR,
	LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR
};

enum class mag_filter : GLint {
	NEAREST = GL_NEAREST,
	LINEAR = GL_LINEAR
};

enum class swizzle : GLint {
	RED = GL_RED,
	GREEN = GL_GREEN,
	BLUE = GL_BLUE,
	ALPHA = GL_ALPHA,
	ZERO = GL_ZERO,
	ONE = GL_ONE
};

enum class wrap : GLint {
	CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE,
	CLAMP_TO_BORDER = GL_CLAMP_TO_BORDER,
	MIRRORED_REPEAT = GL_MIRRORED_REPEAT,
	REPEAT = GL_REPEAT,
#ifdef GL_MIRRORED_CLAMP_TO_EDGE
	MIRRORED_CLAMP_TO_EDGE = GL_MIRRORED_CLAMP_TO_EDGE
#endif
};

enum class internal_format : GLenum {
	STENCIL_INDEX = GL_STENCIL_INDEX,
	RED = GL_RED,
	DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
	DEPTH_STENCIL = GL_DEPTH_STENCIL,
	RG = GL_RG,
	RGB = GL_RGB,
	RGBA = GL_RGBA,
	COMPRESSED_RED = GL_COMPRESSED_RED,
	COMPRESSED_RG = GL_COMPRESSED_RG,
	COMPRESSED_RGB = GL_COMPRESSED_RGB,
	COMPRESSED_RGBA = GL_COMPRESSED_RGBA,
	COMPRESSED_SRGB = GL_COMPRESSED_SRGB,
	COMPRESSED_SRGB_APLHA = GL_COMPRESSED_SRGB_ALPHA
};

enum class image_format : GLenum {
	STENCIL_INDEX = GL_STENCIL_INDEX,
	DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
	DEPTH_STENCIL = GL_DEPTH_STENCIL,

	RED = GL_RED,
	GREEN = GL_GREEN,
	BLUE = GL_BLUE,
	RG = GL_RG,
	RGB = GL_RGB,
	RGBA = GL_RGBA,
	BGR = GL_BGR,
	BGRA = GL_BGRA,

	RED_INTEGER = GL_RED_INTEGER,
	GREEN_INTEGER = GL_GREEN_INTEGER,
	BLUE_INTEGER = GL_BLUE_INTEGER,
	RG_INTEGER = GL_RG_INTEGER,
	RGB_INTEGER = GL_RGB_INTEGER,
	RGBA_INTEGER = GL_RGBA_INTEGER,
	BGR_INTEGER = GL_BGR_INTEGER,
	BGRA_INTEGER = GL_BGRA_INTEGER
};

enum class image_component_type : GLenum {
	BYTE = GL_BYTE,
	SHORT = GL_SHORT,
	INT = GL_INT,
	UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
	UNSIGNED_INT = GL_UNSIGNED_INT,
	HALF_FLOAT = GL_HALF_FLOAT
};

template<GLenum _target>
class texture_target : gl_object_target<texture> {

template<GLenum _name, typename... Ts>
using texture_parameter = gl_object_parameter<texture_parameter_accessors, _target, _name, Ts...>;

template<GLenum _name, typename... Ts>
using texture_parameter_ro = gl_object_parameter<texture_parameter_getters, _target, _name, Ts...>;

public:
	texture_target& operator=(texture & tex) {
		glBindTexture(_target, _getName(tex));
		return (*this);
	}

	texture_target& load_2d(GLint level, internal_format internalFormat, GLsizei width, GLsizei height, GLint border, image_format format, image_component_type type, void const* data) {
		// @todo: see reference for _target
		glTexImage2D(_target, level, static_cast<GLenum>(format), width, height, border, static_cast<GLenum>(format), static_cast<GLenum>(type), data);
		__check_opengl_error();
		return (*this);
	}

	void generate_mipmap() {
		glGenerateMipmap(_target);
	}

	texture_parameter<GL_TEXTURE_WRAP_S, wrap> wrap_s;
	texture_parameter<GL_TEXTURE_WRAP_R, wrap> wrap_r;
	texture_parameter<GL_TEXTURE_WRAP_T, wrap> wrap_t;
	texture_parameter<GL_TEXTURE_BORDER_COLOR, glm::vec4, glm::ivec4> border_color;
	texture_parameter<GL_TEXTURE_MIN_FILTER, min_filter> min_filter;
	texture_parameter<GL_TEXTURE_MAG_FILTER, mag_filter> mag_filter;
	texture_parameter<GL_TEXTURE_LOD_BIAS, GLfloat> lod_bias;
	texture_parameter<GL_TEXTURE_MIN_LOD, GLfloat> min_lod;
	texture_parameter<GL_TEXTURE_MAX_LOD, GLfloat> max_lod;
	texture_parameter<GL_TEXTURE_BASE_LEVEL, GLint> base_level;
	texture_parameter<GL_TEXTURE_MAX_LEVEL, GLint> max_level;
	texture_parameter<GL_TEXTURE_SWIZZLE_A, swizzle> swizzle_a;
	texture_parameter<GL_TEXTURE_SWIZZLE_R, swizzle> swizzle_r;
	texture_parameter<GL_TEXTURE_SWIZZLE_G, swizzle> swizzle_g;
	texture_parameter<GL_TEXTURE_SWIZZLE_B, swizzle> swizzle_b;
	texture_parameter<GL_TEXTURE_SWIZZLE_RGBA, tvec4<swizzle>> swizzle_rgba;
	texture_parameter<GL_TEXTURE_COMPARE_MODE, compare_mode> compare_mode;
	texture_parameter<GL_TEXTURE_COMPARE_FUNC, compare_function> compare_function;
#ifdef GL_STENCIL_COMPONENT
	texture_parameter<GL_DEPTH_STENCIL_TEXTURE_MODE, depth_stencil_texture_mode> depth_stencil_texture_mode;
#endif

	texture_parameter_ro<GL_TEXTURE_IMMUTABLE_FORMAT, GLint> is_immutable_format; // boolean

private:
	texture_target() = default;
	friend class active_texture;
};

class active_texture : non_copyable {
public:
	active_texture& operator=(GLuint texture) {
		if (texture >= GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS) {
			throw std::runtime_error("too big texture number");
		}
		glActiveTexture(GL_TEXTURE0 + texture);
		__check_opengl_error();
		return (*this);
	}

	texture_target<GL_TEXTURE_1D> TEXTURE_1D;
	texture_target<GL_TEXTURE_1D_ARRAY> TEXTURE_1D_ARRAY;
	texture_target<GL_TEXTURE_2D> TEXTURE_2D;
	texture_target<GL_TEXTURE_2D_ARRAY> TEXTURE_2D_ARRAY;
	texture_target<GL_TEXTURE_3D> TEXTURE_3D;
	texture_target<GL_TEXTURE_RECTANGLE> TEXTURE_RECTANGLE;
	texture_target<GL_TEXTURE_BUFFER> TEXTURE_BUFFER;
	texture_target<GL_TEXTURE_CUBE_MAP> TEXTURE_CUBE_MAP;
	texture_target<GL_TEXTURE_CUBE_MAP_ARRAY> TEXTURE_CUBE_MAP_ARRAY;
	texture_target<GL_TEXTURE_2D_MULTISAMPLE> TEXTURE_2D_MULTISAMPLE;
	texture_target<GL_TEXTURE_2D_MULTISAMPLE_ARRAY> TEXTURE_2D_MULTISAMPLE_ARRAY;
private:
	active_texture() = default;

	friend class texture_extension;
};

class texture_extension : public virtual context_extension<> {
public:
	active_texture active_texture;
};

}