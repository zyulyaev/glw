#pragma once

#include "util.hpp"

namespace glw {

typedef void (gl_object_generator)(GLsizei, GLuint*);
typedef void (gl_object_deleter)(GLsizei, GLuint const*);

template<typename T>
using gl_param_getter = void(GLenum, GLenum, T*);
template<typename T>
using gl_param_setter = void(GLenum, GLenum, T);

template<typename> class gl_object;
template<typename> class gl_object_pool;
template<typename> class gl_object_target;

template<typename functions>
class gl_object : non_copyable {
public:
	gl_object(gl_object && object) : _name(object._name), _single(object._single) {
		object._name = 0;
	}

	~gl_object() {
		if (_name != 0 && _single) {
			functions::deleter(1, &_name);
			__check_opengl_error();
		}
	}

	gl_object& operator=(gl_object object) {
		std::swap(_name, object._name);
		std::swap(_single, object._single);
	}

	static gl_object generate() {
		GLuint name;
		functions::generator(1, &name);
		__check_opengl_error();
		return gl_object(name, true);
	}
private:
	gl_object(GLuint name, bool single) : _name(name), _single(single) {}

	GLuint _name;
	bool _single;

	friend class gl_object_pool<gl_object>;
	friend class gl_object_target<gl_object>;
};

template<typename functions>
class gl_object_pool< gl_object<functions> > : non_copyable {
	typedef gl_object<functions> object;

public:
	gl_object_pool(gl_object_pool && pool) :
					_names(std::move(pool._names)),
					_objects(std::move(pool._objects)),
					_invalid(pool._invalid) {
		pool._invalid = true;
	}

	explicit gl_object_pool(GLsizei count) : _names(count), _invalid(false) {
		functions::generator(count, _names.data());
		__check_opengl_error();
		for (GLuint name : _names) {
			_objects.push_back(object(name, false));
		}
	}

	~gl_object_pool() {
		if (!_invalid) {
			functions::deleter(_names.size(), _names.data());
		}
	}

	gl_object_pool& operator=(gl_object_pool pool) {
		std::swap(_names, pool._names);
		std::swap(_objects, pool._objects);
		std::swap(_invalid, pool._invalid);
		return (*this);
	}

	object& operator[](size_t index) {
		_check_index(index);
		return _objects[index];
	}

	object take(size_t index) {
		_check_index(index);
		_names[index] = 0;
		_objects[index]._single = true;
		return std::move(_objects[index]);
	}

private:
	std::vector<GLuint> _names;
	std::vector<object> _objects;
	bool _invalid;

	void _check_index(size_t index) {
#ifdef DEBUG
		if (_names[index] == 0) throw std::runtime_error("object already taken");
#endif
	}
};

template<typename object>
class gl_object_target : non_copyable {
protected:
	gl_object_target() = default;

	static GLuint _getName(object& obj) {
		return obj._name;
	}
};

template<template<typename> class accessors, GLenum _target, GLenum _name, typename... Ts>
class gl_object_parameter : non_copyable {
public:
	gl_object_parameter() = default;

	template<typename T>
	typename std::enable_if<is_any_convertible<T, Ts...>::value, gl_object_parameter&>::type operator=(T value) {
		_set_parameter(value);
		__check_opengl_error();
		return (*this);
	}

	template<typename T>
	operator T() const {
		typename std::enable_if<is_any_convertible<T, Ts...>::value, T>::type value;
		_get_parameter(value);
		__check_opengl_error();
		return value;
	}
private:
	void _set_parameter(GLint value) {
		accessors<GLint>::setter(_target, _name, value);
	}

	void _set_parameter(GLint64 value) {
		accessors<GLint64>::setter(_target, _name, value);
	}

	void _set_parameter(GLfloat value) {
		accessors<GLfloat>::setter(_target, _name, value);
	}

	template<template<typename, glm::precision> class vec, glm::precision p>
	void _set_parameter(vec<GLint, p> value) {
		accessors<GLint>::array_setter(_target, _name, glm::value_ptr(value));
	}

	template<template<typename, glm::precision> class vec, glm::precision p>
	void _set_parameter(vec<GLint64, p> value) {
		accessors<GLint64>::array_setter(_target, _name, glm::value_ptr(value));
	}

	template<template<typename, glm::precision> class vec, glm::precision p>
	void _set_parameter(vec<GLfloat, p> value) {
		accessors<GLfloat>::array_setter(_target, _name, glm::value_ptr(value));
	}

	template<typename E>
	typename std::enable_if<std::is_enum<E>::value>::type _set_parameter(E value) {
		_set_parameter(static_cast<typename std::underlying_type<E>::type>(value));
	}

	template<typename E>
	typename std::enable_if<std::is_enum<E>::value>::type _set_parameter(tvec4<E> value) {
		_set_parameter(static_cast<tvec4<typename std::underlying_type<E>::type>>(value));
	}

	void _get_parameter(GLint & value) const {
		accessors<GLint>::getter(_target, _name, &value);
	}

	void _get_parameter(GLint64 & value) const {
		accessors<GLint64>::getter(_target, _name, &value);
	}

	void _get_parameter(GLfloat & value) const {
		accessors<GLfloat>::getter(_target, _name, &value);
	}

	template<template<typename, glm::precision> class vec, glm::precision p>
	void _get_parameter(vec<GLint, p> & value) const {
		accessors<GLint>::getter(_target, _name, glm::value_ptr(value));
	}

	template<template<typename, glm::precision> class vec, glm::precision p>
	void _get_parameter(vec<GLint64, p> & value) const {
		accessors<GLint64>::getter(_target, _name, glm::value_ptr(value));
	}

	template<template<typename, glm::precision> class vec, glm::precision p>
	void _get_parameter(vec<GLfloat, p> & value) const {
		accessors<GLfloat>::getter(_target, _name, glm::value_ptr(value));
	}

	template<typename E>
	typename std::enable_if<std::is_enum<E>::value>::type _get_parameter(E & value) const {
		typename std::underlying_type<E>::type underlying;
		_get_parameter(underlying);
		value = static_cast<E>(underlying);
	}

	template<typename E>
	typename std::enable_if<std::is_enum<E>::value>::type _get_parameter(tvec4<E> value) const {
		tvec4<typename std::underlying_type<E>::type> underlying;
		_get_parameter(underlying);
		value = static_cast<tvec4<E>>(underlying);
	}
};

}