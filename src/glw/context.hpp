#pragma once

#include "util.hpp"

template<typename... extensions>
class context : public extensions... {
public:

private:

};

template<typename... dependencies>
class context_extension : public virtual dependencies... {
public:
	context_extension() = default;
private:

};