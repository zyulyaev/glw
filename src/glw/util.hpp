#pragma once

#include <GL/glew.h>

#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <stdexcept>
#include <vector>
#include <type_traits>

namespace glw {

namespace {

	std::string __get_opengl_error_string(GLenum error) {
		switch (error) {
			case GL_NO_ERROR: return "NO_ERROR";
			case GL_INVALID_ENUM: return "GL_INVALID_ENUM";
			case GL_INVALID_VALUE: return "GL_INVALID_VALUE";
			case GL_INVALID_OPERATION: return "GL_INVALID_OPERATION";
			case GL_STACK_OVERFLOW: return "GL_STACK_OVERFLOW";
			case GL_STACK_UNDERFLOW: return "GL_STACK_UNDERFLOW";
			case GL_OUT_OF_MEMORY: return "GL_OUT_OF_MEMORY";
			case GL_INVALID_FRAMEBUFFER_OPERATION: return "GL_INVALID_FRAMEBUFFER_OPERATION";
		}
		return "Unknown OpenGL error";
	}

	void __check_opengl_error() {
	#ifdef DEBUG
		GLenum error = glGetError();
		if (error) {
			throw std::runtime_error(__get_opengl_error_string(error));
		}
	#endif
	}

	template<typename T>
	void __vararg_build(std::vector<T>& vec) {}

	template<typename T, typename Arg, typename... Args>
	void __vararg_build(std::vector<T>& vec, Arg arg, Args... args) {
		vec.push_back(std::move(arg));
		__vararg_build<T>(vec, std::move(args)...);
	}

	template<typename T, typename... Args>
	std::vector<T> __vararg(Args... args) {
		std::vector<T> res;
		__vararg_build<T>(res, std::move(args)...);
		return res;
	}
}

class non_copyable {
public:
	non_copyable(non_copyable const&) = delete;
	non_copyable& operator=(non_copyable const&) = delete;
protected:
	non_copyable() = default;
};

template<bool... values>
struct if_any;

template<bool... values>
struct if_any<true, values...> {
	static constexpr bool value = true;
};

template<bool... values>
struct if_any<false, values...> : if_any<values...> {};

template<>
struct if_any<> {
	static constexpr bool value = false;
};

template<bool... values>
struct if_all;

template<bool... values>
struct if_all<false, values...> {
	static constexpr bool value = false;
};

template<bool... values>
struct if_all<true, values...> : if_all<values...> {};

template<>
struct if_all<> {
	static constexpr bool value = true;
};

template<typename T> using tvec4 = glm::detail::tvec4<T, glm::highp>;

template<typename F, typename... Ts>
struct is_any_convertible {
	static constexpr bool value = if_any<std::is_convertible<F, Ts>::value...>::value;
};

template<typename F, typename T>
T reduce(F lambda, T initial) {
	return initial;
}

template<typename F, typename T, typename... Ts>
T reduce(F lambda, T initial, T first, Ts... values) {
	return reduce(lambda, lambda(initial, first), values...);
}

}