#pragma once

#include "util.hpp"
#include "context.hpp"

namespace glw {

template<GLenum _name> class gl_capability;
template<GLenum _name> class gl_single_capability;
template<GLenum _name> class gl_indexed_capability;
class capability_extension;

template<GLenum _name>
class gl_capability : non_copyable {
public:
	gl_capability& operator=(GLboolean value) {
		if (value) {
			glEnable(_name);
		} else {
			glDisable(_name);
		}
		__check_opengl_error();
		return (*this);
	}

	operator GLboolean() const {
		return glIsEnabled(_name);
	}
};

template<GLenum _name>
class gl_single_capability {
public:
	gl_single_capability& operator=(GLboolean value) {
		if (value) {
			glEnablei(_name, _index);
		} else {
			glDisablei(_name, _index);
		}
		__check_opengl_error();
		return (*this);
	}

	operator GLboolean() const {
		return glIsEnabledi(_name, _index);
	}

private:
	gl_single_capability(GLuint index) : _index(index) {}

	GLuint _index;

	friend class gl_indexed_capability<_name>;
};

template<GLenum _name>
class gl_indexed_capability : non_copyable {
public:
	gl_single_capability<_name> operator[](GLint index) {
		return index;
	}

	gl_indexed_capability& operator=(GLboolean value) {
		if (value) {
			glEnable(_name);
		} else {
			glDisable(_name);
		}
		__check_opengl_error();
		return (*this);
	}

	operator GLboolean() const {
		return glIsEnabled(_name);
	}
};

}