#pragma once

#include "context.hpp"

#include "vbo.hpp"
#include "vao.hpp"
#include "shader.hpp"
#include "texture.hpp"
#include "capability.hpp"
#include "framebuffer.hpp"

namespace glw {

context<
	shader_extension,
	texture_extension,
	vertex_array_extension,
	buffer_extension,
	framebuffer_extension
> ctx;

}