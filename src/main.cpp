#include "glw/glw.hpp"
#include "res.hpp"

#include <GLFW/glfw3.h>
#include <SDL2/SDL_image.h>

#include <memory>
#include <chrono>
#include <iostream>

#include <glm/ext.hpp>


namespace sdl {

class window : glw::non_copyable {
public:
	window(std::string const& title, int width, int height) {
		glfwInit();

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		_window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
		glfwMakeContextCurrent(_window);

		glewExperimental = true;
		glewInit();

		glGetError(); // clear error flag
	}

	window(window && window) : _window(window._window) {
		window._window = nullptr;
	}

	window& operator=(window window) {
		std::swap(_window, window._window);
		return (*this);
	}

	~window() {
		if (_window != nullptr) {
			glfwDestroyWindow(_window);
			glfwTerminate();
		}
	}

	void display() {
		glfwSwapBuffers(_window);
	}

	bool is_closed() {
		return glfwWindowShouldClose(_window);
	}

	void process_events() {
		glfwPollEvents();
	}
private:
	GLFWwindow* _window;
};

}

int main() {
	sdl::window window("OpenGL", 800, 600);

	auto& context = glw::ctx;

	glw::vertex_array vao = glw::vertex_array::generate();
	context.active_vertex_array = vao;

	glw::buffer_pool buffer_pool(1);

	glw::buffer& pts_buffer = buffer_pool[0];

	GLfloat vertices[] = {
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,

        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f,

        -1.0f, -1.0f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
         1.0f, -1.0f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
         1.0f,  1.0f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
         1.0f,  1.0f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
        -1.0f,  1.0f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
        -1.0f, -1.0f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f
	};

	context.buffer_array = pts_buffer;
	context.buffer_array.loadData(sizeof(vertices), vertices, glw::buffer_usage::STATIC_DRAW);

	glw::shader_program program = glw::shader_program::link(
		glw::shader::compile(glw::shader_type::VERTEX, res::shader::vertex.data),
		glw::shader::compile(glw::shader_type::FRAGMENT, res::shader::fragment.data)
	);
	context.active_program = program;

	context.active_vertex_array
		.enable_vertex_attrib_array(program.get_attrib_location("pos"))
		.set_vertex_attrib_pointer(program.get_attrib_location("pos"), 3, glw::component_type::FLOAT, false, 8 * sizeof(GLfloat), nullptr)

		.enable_vertex_attrib_array(program.get_attrib_location("color"))
		.set_vertex_attrib_pointer(program.get_attrib_location("color"), 3, glw::component_type::FLOAT, false, 8 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)))

		.enable_vertex_attrib_array(program.get_attrib_location("texcoord"))
		.set_vertex_attrib_pointer(program.get_attrib_location("texcoord"), 2, glw::component_type::FLOAT, false, 8 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));

	glw::texture_pool textures(2);

	context.active_texture = 0;
	context.active_texture.TEXTURE_2D = textures[0];
	SDL_Surface* kitten = IMG_Load_RW(SDL_RWFromConstMem(res::image::kitten.data, res::image::kitten.length), 1);
	context.active_texture.TEXTURE_2D.load_2d(
			0,
			kitten->format->BytesPerPixel == 4 ? glw::internal_format::RGBA : glw::internal_format::RGB,
			kitten->w,
			kitten->h,
			0,
			kitten->format->BytesPerPixel == 4 ? glw::image_format::RGBA : glw::image_format::RGB,
			glw::image_component_type::UNSIGNED_BYTE,
			kitten->pixels
		);
	context.active_texture.TEXTURE_2D.wrap_s = glw::wrap::CLAMP_TO_EDGE;
	context.active_texture.TEXTURE_2D.wrap_t = glw::wrap::CLAMP_TO_EDGE;
	context.active_texture.TEXTURE_2D.min_filter = glw::min_filter::LINEAR;
	context.active_texture.TEXTURE_2D.mag_filter = glw::mag_filter::LINEAR;
	program.set_uniform(program.get_uniform_location("texKitten"), 0);
	SDL_FreeSurface(kitten);

	context.active_texture = 1;
	context.active_texture.TEXTURE_2D = textures[1];
	SDL_Surface* puppy = IMG_Load_RW(SDL_RWFromConstMem(res::image::puppy.data, res::image::puppy.length), 1);
	context.active_texture.TEXTURE_2D.load_2d(
			0,
			puppy->format->BytesPerPixel == 4 ? glw::internal_format::RGBA : glw::internal_format::RGB,
			puppy->w,
			puppy->h,
			0,
			puppy->format->BytesPerPixel == 4 ? glw::image_format::RGBA : glw::image_format::RGB,
			glw::image_component_type::UNSIGNED_BYTE,
			puppy->pixels
		);
	context.active_texture.TEXTURE_2D.wrap_s = glw::wrap::CLAMP_TO_BORDER;
	context.active_texture.TEXTURE_2D.wrap_t = glw::wrap::CLAMP_TO_EDGE;
	context.active_texture.TEXTURE_2D.min_filter = glw::min_filter::LINEAR;
	context.active_texture.TEXTURE_2D.mag_filter = glw::mag_filter::LINEAR;
	program.set_uniform(program.get_uniform_location("texPuppy"), 1);
	SDL_FreeSurface(puppy);

	program
		.set_uniform(
			program.get_uniform_location("proj"),
			glm::perspective(45.0f, 800.0f / 600.0f, 1.0f, 10.0f)
		).set_uniform(
			program.get_uniform_location("view"),
			glm::lookAt(
				glm::vec3(2.5f, 2.5f, 2.0f),
		        glm::vec3(0.0f, 0.0f, 0.0f),
		        glm::vec3(0.0f, 0.0f, 1.0f)
			)
		);

	context.framebuffer.clear_color = {1, 1, 1, 1};
	context.framebuffer.depth_test.enabled = true;

	while (!window.is_closed())
	{
		window.process_events();

		context.framebuffer.clear(glw::clear_buffer::COLOR, glw::clear_buffer::DEPTH);

		glm::mat4 model = glm::rotate(
            std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()  * 0.180f,
            glm::vec3(0.0f, 0.0f, 1.0f)
        );
        program.set_uniform(program.get_uniform_location("model"), model);

		context.active_vertex_array.draw_arrays(glw::drawing_mode::TRIANGLES, 0, 36);

		context.framebuffer.stencil_test.enabled = true;

			context.framebuffer.stencil_test
						.set_function(glw::compare_function::ALWAYS, 1, 0xFF)
						.set_operation(glw::stencil_operation::KEEP, glw::stencil_operation::KEEP, glw::stencil_operation::REPLACE)
						.set_mask(0xFF);
			context.framebuffer.depth_test.set_mask(GL_FALSE);
			context.framebuffer.clear(glw::clear_buffer::STENCIL);

			context.active_vertex_array.draw_arrays(glw::drawing_mode::TRIANGLES, 36, 6);

			context.framebuffer.stencil_test
						.set_function(glw::compare_function::EQUAL, 1, 0xFF)
						.set_mask(0x00);
			context.framebuffer.depth_test.set_mask(GL_TRUE);

			program.set_uniform(program.get_uniform_location("model"), glm::scale(glm::translate(model, glm::vec3(0, 0, -1)), glm::vec3(1, 1, -1)));

			program.set_uniform(program.get_uniform_location("overrideColor"), glm::vec3(0.3f, 0.3f, 0.3f));
				context.active_vertex_array.draw_arrays(glw::drawing_mode::TRIANGLES, 0, 36);
			program.set_uniform(program.get_uniform_location("overrideColor"), glm::vec3(1.f, 1.f, 1.f));

		context.framebuffer.stencil_test.enabled = false;

		window.display();
	}

	return 0;
}