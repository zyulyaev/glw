CC=clang++
STD=-std=c++11
LIBS=-framework OpenGL -lglew -lglfw3 -lSDL2 -lSDL2_image
DEBUG_FLAGS=-DDEBUG -g -Wall

SRC=./src
GEN=./gen
BUILD=./build
RES=./res

SOURCES=$(SRC)/main.cpp
EXECUTABLE=$(BUILD)/main

RES_LIST=$(SRC)/res.list
RES_OBJ=$(BUILD)/res.o
RES_SRC=$(GEN)/res.s
RES_HPP=$(GEN)/res.hpp

ASMBL_SRC=$(SRC)/asmbl.cpp
ASMBL_EXEC=$(BUILD)/asmbl

ASM=yasm
ASM_FORMAT=-f macho64

all: resources
	$(CC) $(STD) $(LIBS) $(DEBUG_FLAGS) -o $(EXECUTABLE) -I $(GEN) $(RES_OBJ) $(SOURCES)

opt: resources
	$(CC) $(STD) $(LIBS) -O3 -o $(EXECUTABLE) -I $(GEN) $(RES_OBJ) $(SOURCES)

resources: asmbl
	$(ASMBL_EXEC) $(RES_SRC) $(RES_HPP) < $(RES_LIST)
	$(ASM) $(ASM_FORMAT) -o $(RES_OBJ) -I $(SRC) -I $(RES) $(RES_SRC)

asmbl:
	$(CC) $(STD) -o $(ASMBL_EXEC) $(ASMBL_SRC)

clean:
	rm -rf $(BUILD)/* $(GEN)/*

run:
	$(EXECUTABLE)